FILE_NAME_1 = "player1.txt"
FILE_NAME_2 = "player2.txt"
RESULT_FILE = "result.txt"

def compare(ch1, ch2):

    if ch1 == ch2 :
        return 0

    if (ch1 == "R" and ch2 == "S") or \
        (ch1 == "P" and ch2 == "R") or \
        (ch1 == "S" and ch2 == "P"):
        return 1

    return 2

def read_from_files(file_name_1, file_name_2):
    results = [0, 0, 0]

    with open(file_name_1, "r") as file_1, open(file_name_2, "r") as file_2:
        while True:
            line_1 = file_1.readline().strip()
            line_2 = file_2.readline().strip()

            if not line_1 and not line_2:
                break

            results[compare(line_1, line_2)]+=1

    return results

def write_to_file(file_name, results):
    with open(file_name, "w+") as file:
        file.write("Player1 wins: " + str(results[1]) + "\n")
        file.write("Player2 wins: " + str(results[2]) + "\n")
        file.write("Draws: " + str(results[0]) + "\n")

def main():
    results = read_from_files(FILE_NAME_1, FILE_NAME_2)
    write_to_file(RESULT_FILE, results)

if __name__ == "__main__":
    main()
